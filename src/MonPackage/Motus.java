package MonPackage ; 
import java.util.Scanner;
public class Motus {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
      // Demander au premier utilisateur de saisir un mot
        System.out.println("Joueur 1, veuillez saisir un mot d'au moins 4 lettres:");
        String mot = sc.nextLine();
        
        // Vérifier que le mot a au moins 4 lettres
        while (mot.length() < 4) {
            System.out.println("Le mot doit contenir au moins 4 lettres. Veuillez réessayer:");
            mot = sc.nextLine();
        }
        
        // Initialiser la chaîne de caractères qui sera affichée à l'utilisateur
        StringBuilder display = new StringBuilder();
        for (int i = 0; i < mot.length(); i++) {
            display.append("_");
        }
        
        // Initialiser le nombre d'essais restants
        int remainingTries = mot.length() - 2;
        
        // Boucle pour jouer jusqu'à ce que le mot soit deviné ou qu'il n'y ait plus d'essais
        while (remainingTries > 0 && display.toString().contains("_")) {
            // Afficher l'état actuel du mot
            System.out.println(display);
            
            // Demander à l'utilisateur de deviner une lettre
            System.out.println("Joueur 2, veuillez saisir une lettre:");
            String guess = sc.nextLine().toLowerCase();
            
            // Vérifier que l'entrée est une seule lettre
            while (guess.length() != 1) {
                System.out.println("Veuillez saisir une seule lettre:");
                guess = sc.nextLine().toLowerCase();
            }
            
            // Vérifier si la lettre devinée est dans le mot
            boolean found = false;
            for (int i = 0; i < mot.length(); i++) {
                if (mot.charAt(i) == guess.charAt(0)) {
                    found = true;
                    display.setCharAt(i, guess.charAt(0));
                }
            }
         // Diminuer le nombre d'essais restants si la lettre n'est pas dans le mot
            if (!found) {
                remainingTries--;
                System.out.println("La lettre n'est pas dans le mot. Il vous reste " + remainingTries + " essais.");
            }
            
            // Donner un indice au joueur s'il lui reste seulement un essai
            if (remainingTries == 1) {
                int randomIndex = (int) (Math.random() * mot.length());
                while (display.charAt(randomIndex) != '_') {
                    randomIndex = (int) (Math.random() * mot.length());
                }
                System.out.println("Indice: la lettre à la position " + (randomIndex + 1) + " est " + mot.charAt(randomIndex));
            }
        }
        
        // Afficher le résultat final
        if (!display.toString().contains("_")) {
            System.out.println("Félicitations! Vous avez deviné le mot \"" + mot + "\".");
        } else {
            System.out.println("Dommage! Vous n'avez pas réussi à deviner le mot \"" + mot + "\".");
        }
        
        sc.close();
    }
}